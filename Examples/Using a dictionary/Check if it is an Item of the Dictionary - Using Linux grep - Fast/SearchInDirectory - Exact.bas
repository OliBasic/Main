REM SearchInDictionary.bas
FN.DEF InsertInMyDic(path$, dic$, insertText$)
 !* Creates/opens a additional dictionary.
 BYTE.OPEN a, bft, "file://" + path$ + "/" + dic$
 BYTE.WRITE.BUFFER bft, " " + ENCODE$("URL", "ISO-8859-1", insertText$) + "\n"
 BYTE.CLOSE bft
 FN.RTN 1
FN.END
FN.DEF GrepSearch$(path$, dic$, modifiers$, searchText$ )
 searchText$ = ENCODE$("URL", "ISO-8859-1", searchText$)
 !* You can use grep with Android >= 4.1-4.3 (JellyBean)
 d$ =  "grep " + modifiers$ + "'" + searchText$ + "' " + path$ + "/" + dic$ +"\n"
 BYTE.OPEN w, bft, "file://" + path$ + "/" + "script.sh"
 BYTE.WRITE.BUFFER bft, d$
 BYTE.CLOSE bft
 d$ = "sh " + path$ + "/" + "script.sh"
 SHELL text$, d$
 !IF text$ = "" THEN text$ = "Empty Result!"
 FN.RTN text$
FN.END
FN.DEF SearchItem(path$, dic$, shortDic$, myDic$, searchText$)
 t1 =TIME()
 modifiers$ = "-w -e "% -w for complete words, -e search pattern follows
 text$ = ""
 FILE.EXISTS ok, "file://" + path$ + "/" + myDic$
 !* Insert LET, because TEXT is a reserved key word.
 !* The additional dictionary is the first choice.
 IF ok > 0 THEN LET text$ = text$ + GrepSearch$(path$, myDic$, modifiers$, searchText$)
 !* Returns often more items, because URL encoded characters
 IF text$ = "" THEN LET text$ = text$ + GrepSearch$(path$, shortDic$, modifiers$, searchText$)
 IF text$ = "" THEN LET text$ = text$ + GrepSearch$(path$, dic$, modifiers$, searchText$)
 t2 =TIME()
 PRINT "Grep search time: ", (t2-t1)/1000

 SPLIT.ALL resultArray$[], text$,  CHR$(10)
 LIST.CREATE S, matchResults
 LIST.ADD.ARRAY matchResults, resultArray$[]
 LIST.SIZE matchResults, ls
 FOR i = 1 TO ls
  LIST.GET matchResults, i, text$
  text$ = TRIM$(DECODE$("URL", "ISO-8859-1", text$))
  LIST.REPLACE matchResults, i, text$
 NEXT
 t2 =TIME()
 LIST.SEARCH matchResults, searchText$, w
 DEBUG.ON
 DEBUG.DUMP.LIST matchResults
 PRINT w
 IF w > 0
  LIST.GET matchResults, w, text$
  PRINT text$
 ENDIF
 PRINT "Complete search time: ", (t2-t1)/1000
 FN.RTN w
FN.END

BUNDLE.PUT clbp, "_TextFont", "_Default" % This line is a must!
BUNDLE.PUT clbp, "_TextColor", "255,0,0,0"
BUNDLE.PUT clbp, "_TextBackgroundColor", "0,0,0,0"
BUNDLE.PUT clbp, "_BackgroundColor", "255,255,140,0"
BUNDLE.PUT clbp, "_DividerColor", "255,0,0,0"
BUNDLE.PUT clbp, "_DividerHeight", 3
Console.layout clbp

dic$ = "germanSpaceURL.dic" % Characters outside ASCII code are encryptetd as URL code, 
!^ because they will be ignored in Android's shell implementation.
!^ In this case according to the ISO-8859-1 standard.
!^ Mostly also "UTF-8" is possible. But the character set has to be overall the same in the commands.
!^ The first character has to be a white space, because BSD grep is not fully/correct implemented.
shortDic$ = "shortGermanSpaceURL.dic" % For very short words like "in" 
myDic$ = "myDicSpaceURL.dic" % For your own stuff

FILE.ROOT path$ % Basic!'s data path
!searchText$ = "auf"
!w = SearchItem(path$, dic$, searchText$)
DO
 INPUT  "Search text" , searchText$ 
 IF LEN(searchText$) > 1 % For words longer than 1 letter. Handle a, $, € ... in a special routine.
  w = SearchItem(path$, dic$, shortDic$, myDic$, searchText$)
  FILE.EXISTS ok, "file://" + path$ + "/" + myDic$
  IF w = 0
   dispText$ = "Should   " + searchText$ + "   be inserted?"
   DIALOG.MESSAGE "", dispText$, sel, "Go on", "Close", "Insert"
  ELSE
   dispText$ = "Item  " + INT$(w) + "  in the list is the right one."
   DIALOG.MESSAGE "", dispText$, sel, "Go on", "Close"
  ENDIF
  IF sel = 3 THEN InsertInMyDic(path$, myDic$, searchText$)
 ENDIF
UNTIL sel = 2
END
